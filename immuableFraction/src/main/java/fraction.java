public class fraction {
    int num;
    int denum;
    private final static fraction ZERO;
    private final static fraction UN;
    static {
        ZERO = new fraction(0,1);
        UN = new fraction(1,1);
    }
    fraction(int num ,int denum){
        this.num = num;
        this.denum = denum;
    }

    fraction(int num ){
        this.num = num;
        this.denum = 1;
    }

    fraction(){
        this.num = 0;
        this.denum = 1;
    }

    public int getNum(){
        return num;
    }

    public int getDenum(){
        return denum;
    }

    public double getFract(){
        return num/denum;
    }

    public fraction FracAddition(fraction frac){

        int tempNumFrac = frac.getNum()*this.denum;
        int tempNum = this.num*frac.getDenum();
        int TempDenum = frac.getDenum() * this.denum;
        return new fraction(tempNumFrac  + tempNum,TempDenum);
    }

    @Override
    public String toString(){
        return "Numerateur : " + num + " Denumerateur : " + denum;
    }

    public String comparaison(fraction frac) {
        if (this.num/this.denum < frac.getNum()/frac.getDenum()) {
            return "Plus petit";
        } else {
            if (this.num/this.denum > frac.getNum()/frac.getDenum()) {
                return "Plus grand";
            }
                else {
                    return "Plus égale";
                }
            }
    }

    public boolean egalite(fraction frac){
        if(this.num/this.denum == frac.getNum()/frac.getDenum()){
            return true;
        }
        else
            return false;
    }

}