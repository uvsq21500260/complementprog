/**
 *
 Gestion de la classe compte bancaire
 *
 * @version
1.0 oct 2017  * @author
SOTTAS Jean-Charles  */
public class Compte {


    /**
     *Variable solde sauvegarde l'argent sur le compte
     */
    private int Solde;

    /* Implementation de la class Compte. */
    /**
     *Initialise le compte avec un montant initial
     * @param solde Le montant du compte initial
     * @throws ExeptionNegativeSolde
     */
    Compte(int solde)throws  ExeptionNegativeSolde{
        if(solde >=0) {
            this.Solde = solde;
        }else {
            throw new ExeptionNegativeSolde();
        }
    }

    /* A class implementation comment can go here. */

    /**
     * Permet de retourner le montant du compte
     * @return Solde
     */
    public int Consultation(){
        return Solde;
    }
    /* A class implementation comment can go here. */

    /**
     * Permet d'ajout de l'argent sur le compte
     * @param credit Le montant a Ajouter
     * @throws ExeptionNegativeSolde Exeption si l'argent passer en argument est negatif
     */
    public void Credit(int credit) throws ExeptionNegativeSolde{
        if (credit <0)throw new ExeptionNegativeSolde();
        this.Solde = Solde + credit;
    }
    /* A class implementation comment can go here. */

    /**
     *permet d'enlever de l'argent sur le compte
     * @param debit Le montant a debiter
     * @throws ExeptionNegativeDebit Exeption si l'argent devient inferieur a zero
     * @throws ExeptionNegativeSolde Exeption si l'argent passer en argument est negatif
     */
    public void Debit(int debit)throws ExeptionNegativeDebit,ExeptionNegativeSolde{

        if(debit < 0) throw new ExeptionNegativeSolde();
        if (Solde-debit >=0){
            this.Solde = Solde - debit;}
        else {

            throw new ExeptionNegativeDebit();
        }
    }

    /* A class implementation comment can go here. */

    /**
     * Permet de faire un viement entre deux comptes bancaires
     * @param compte Passe en argument un compte bancaire pour faire le virement
     * @param montant Donne le montant a virer
     * @return Compte Le compte apres le virement
     * @throws ExeptionNegativeDebit Exeption si l'argent devient inferieur a zero
     * @throws ExeptionNegativeSolde Exeption si l'argent passer en argument est negatif
     */
    public Compte virement (Compte compte,int montant) throws ExeptionNegativeDebit,ExeptionNegativeSolde{

        if(montant <0)throw new ExeptionNegativeSolde();
        if((Solde - montant)< 0)throw new ExeptionNegativeDebit();
        this.Solde = Solde - montant;
        compte.Solde = compte.Solde +montant;
        return compte;
    }
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
