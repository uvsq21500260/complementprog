public class fichier extends arborescence {
  int taille;

  /**
   * Constructeur de la classe fichier
   * @param name le nom du fichier
   * @param taille la taille du fichier
   */
  public fichier(String name,int taille) {
    this.taille = taille;
    this.name = name;
  }

  /**
   * retourne la taille du fichier
   * @return la taille du fichier
   */
  public int calculTaille() {
    return taille;
  }

  /**
   * Test s'il est possible de rajouté le dossier dans l'arborescence
   * @param nom le nom du fichier
   * @return true car pas de contrainte avec le fichier sur l'ajout du dossier
   */
  protected boolean VerfifieNom(String nom) {
    return true;
  }
}
