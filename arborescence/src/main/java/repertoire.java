import java.util.ArrayList;
import java.util.List;

public class repertoire extends arborescence {

    private List<arborescence>reper;
    /**
     * Constructor for objects of class repertoire
     */
    public repertoire(final String name)
    {
        this.name=name;
        reper=new ArrayList<arborescence>();
    }

    /**
     * test s'il est possible de le rajouter
     * @param nom une arborescence que nous allons verifie
     */
    public void ajouter(arborescence nom)
    {
        if(this.name == nom.name){return;}
        else{/*
            for(int i=0;i<reper.size();i++){
                 if(!reper.get(i).VerfifieNom(nom.name)){
                     return;
                }
            }*/
            if(!nom.VerfifieNom(name)){
                return;
            }
        }
        reper.add(nom);
    }


    /**
     * redefinition de la classe toString
     * @return
     */
    @Override
    public String toString() {
        return "taille : " + calculTaille();
    }

    /**
     * Calcule la taille du dossier
     * @return la taille
     */
    protected int calculTaille() {
        int tmp = 0;
        for(int i=0;i<reper.size();i++){
            tmp += reper.get(i).calculTaille();
        }
        return tmp;
    }

    /**
     *  parcoure l'arborescence pour verfie qu'il ne se rajoute pas lui même
     * @param a Le nom du dossier
     * @return true s'il peut le rajoute sinon false
     */
    protected boolean VerfifieNom(String a) {

        if(a == name){
            return false;}
        for(int i=0;i<reper.size();i++){
            if(!reper.get(i).VerfifieNom(a)){
                return false;
            }
        }
        return true;
    }
}
