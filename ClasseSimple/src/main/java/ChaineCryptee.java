import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

public class ChaineCryptee {
    String chaine;
    int declage;

    /**
     * Constructeur de la classe
     * @param chaine chaine à crypter
     * @param decalage le decalage utilisé pour le cryptage
     */
    private ChaineCryptee (String chaine, int decalage){
        if(chaine == null){
            chaine = "";
        }
        this.chaine = chaine;
        this.declage = decalage;
        this.chaine = crypte();
        System.out.println(chaine);

    }

    public static ChaineCryptee deCryptee(String c, int d){
        ChaineCryptee Ch = new ChaineCryptee(c,26-(d%26));
        ChaineCryptee C = new ChaineCryptee(Ch.chaine,d);
        return C;
    }

    public static ChaineCryptee deEnClair(String c, int d){
        return new ChaineCryptee(c,d);
    }
    /**
     * FOnction qui decale un caractere suivant le decalage
     * @param c le caractère
     * @param decalage le décalage
     * @return
     */
    private static char decaleCaractere(char c, int decalage) {
        return (c < 'A' || c > 'Z')? c : (char)(((c - 'A' + decalage) % 26) + 'A');
    }

    /**
     * retourne la chaine decrypter
     * Si chaine = null le programme retourne une chaine vide
     * @return chaine décrypter
     */
    public String decrypte(){
        StringBuilder C = new StringBuilder();
        for(int i = 0; i<chaine.length(); i++){
            C=C.insert(i,decaleCaractere(chaine.charAt(i),26-(declage%26)));
        }
        return C.toString();
    }
    /**
     * retourne la chaine crypter
     * Si crypter = null le programme retourne une chaine vide
     * @return chaine crypter
     */
    public String crypte(){

        StringBuilder C = new StringBuilder();
        for(int i = 0; i<chaine.length(); i++){
            C=C.insert(i,decaleCaractere(chaine.charAt(i),declage));
        }
        return C.toString();
    }

}
